;; global variables

(setq
 inhibit-startup-screen t
 create-lockfiles nil
 make-backup-files nil
 column-number-mode t
 scroll-error-top-bottom t
 show-paren-delay 0.2
 use-package-always-ensure t
 sentence-end-double-space nil)

(global-linum-mode 1)

;; buffer local variables
(setq-default
 indent-tabs-mode nil
 tab-width 2
 c-basic-offset 2)

;; the package manager
(require 'package)

(setq
 package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                    ("org" . "http://orgmode.org/elpa/")
                    ("melpa" . "http://melpa.org/packages/")
                    ("melpa-stable" . "http://stable.melpa.org/packages/"))
 package-archive-priorities '(("melpa-stable" . 1)))

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)


(use-package ensime
  :ensure t
  :pin melpa-stable)

(use-package projectile
  :demand
  :init   (setq projectile-use-git-grep t)
  :config (projectile-global-mode t)
  :bind   (("s-f" . projectile-find-file)
           ("s-F" . projectile-grep)))

(use-package undo-tree
  :diminish undo-tree-mode
  :config (global-undo-tree-mode)
  :bind ("C-S-t" . undo-tree-visualize))

(use-package flx-ido
  :demand
  :init
  (setq
   ido-enable-flex-matching t
   ido-show-dot-for-dired nil
   ido-enable-dot-prefix t)
  :config
  (ido-mode 1)
  (ido-everywhere 1)
  (flx-ido-mode 1))

(use-package highlight-symbol
  :diminish highlight-symbol-mode
  :commands highlight-symbol
  :bind ("C-S-h" . highlight-symbol)
)

(use-package goto-chg
  :commands goto-last-change
  :bind (("C-." . goto-last-change)
         ("C-," . goto-last-change-reverse)))

(use-package popup-imenu
  :commands popup-imenu
  :bind ("M-i" . popup-imenu))

(use-package magit
  :commands magit-status magit-blame
  :init (setq
         magit-revert-buffers nil))


(use-package expand-region 
:init
(progn
(require 'expand-region)
)
)

;; the great helm mode
(use-package helm
  :diminish helm-mode
  :init
  (progn
    (require 'helm-config)
    (setq helm-candidate-number-limit 100)
    ;; From https://gist.github.com/antifuchs/9238468
    (setq helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
          helm-input-idle-delay 0.01  ; this actually updates things
                                        ; reeeelatively quickly.
          helm-yas-display-key-on-candidate t
          helm-quick-update t
          helm-M-x-requires-pattern nil
          helm-ff-skip-boring-files t
          helm-autoresize-mode t)
    (helm-mode))
  :bind (("C-c h" . helm-projectile)
         ("C-h a" . helm-apropos)
         ("C-x C-b" . helm-buffers-list)
         ("C-x b" . helm-buffers-list)
         ("M-y" . helm-show-kill-ring)
         ("M-x" . helm-M-x)
         ("C-x c o" . helm-occur)
         ("C-x c s" . helm-swoop)
         ("C-x c y" . helm-yas-complete)
         ("C-x c Y" . helm-yas-create-snippet-on-region)
         ("C-x c b" . my/helm-do-grep-book-notes)
         ("C-x c SPC" . helm-all-mark-rings)))

(use-package helm-projectile
  :init
  (progn
    (require 'helm-projectile)
    (helm-projectile-on))
  :bind (("C-x t" . helm-projectile)))

;; move lines with ease
(use-package drag-stuff
  :init
  (progn
    (drag-stuff-mode t)
    (drag-stuff-define-keys)
    (drag-stuff-global-mode 1)))

(use-package smooth-scrolling
  :init
  (progn
    (require 'smooth-scrolling)
    (smooth-scrolling-mode 1)))

(use-package persistent-scratch
  :init
  (progn
    (persistent-scratch-setup-default)))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package better-shell
    :ensure t
    :bind (("C-'" . better-shell-shell)
           ("C-;" . better-shell-remote-open)))




;;(use-package git-gutter)
;;(global-git-gutter-mode +1)

;; setting shortcut to find file
(fset `yes-or-no-p `y-or-n-p)

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-c d") 'kill-whole-line)
(global-set-key (kbd "C-x e") 'ensime)
(global-set-key (kbd "C-x o") 'ensime-refactor-organize-imports)
(global-set-key (kbd "C-x a") 'org-capture)
(global-set-key (kbd "<C-up>") 'shrink-window)
(global-set-key (kbd "<C-down>") 'enlarge-window)
(global-set-key (kbd "<C-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<C-right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)
(global-set-key (kbd "<C-c h>") 'helm-projectile)
(global-set-key (kbd "C-c l") 'scalafmt-file)
(global-set-key (kbd "C-=") 'er/expand-region)

;; org mode capture 

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
(setq org-capture-templates
 '(("t" "Todo" entry (file+headline "~/.emacs.d/gtd.org" "Tasks")
        "* TODO %?\n  %i\n  ")
   ("m" "Meeting" entry (file+datetree "~/.emacs.d/meeting.org")
        "* %?\nEntered on %U\n  %i\n ")
   ("j" "Journal" entry (file+datetree "~/.emacs.d/journal.org")
        "* %?\nEntered on %U\n  %i\n ")
   ("b" "Blog Items" entry (file+datetree "~/.emacs.d/blog.org")
        "* %?\nEntered on %U\n  %i\n ")
))

;; changing the command to control mac specific 
(setq mac-command-modifier 'control)

(setq mac-option-modifier 'meta)

(setq explicit-shell-file-name "/usr/local/bin/fish")

(defun oleh-term-exec-hook ()
  (let* ((buff (current-buffer))
         (proc (get-buffer-process buff)))
    (set-process-sentinel
     proc
     `(lambda (process event)
        (if (string= event "finished\n")
            (kill-buffer ,buff))))))

(defun scalafmt-file ()
  (interactive)
  (let ((str (concat "scalafmt -f " buffer-file-name " --config=/Users/prassee/.scalafmt.conf -i --exclude ensime")))
    (message str)
    (shell-command-to-string str))
  (revert-buffer-no-confirm)
  (message "scalafmt done"))

;; Source: http://www.emacswiki.org/emacs-en/download/misc-cmds.el
(defun revert-buffer-no-confirm ()
    "Revert buffer without confirmation."
    (interactive)
    (revert-buffer :ignore-auto :noconfirm))


(add-hook 'term-exec-hook 'oleh-term-exec-hook)

;; mac specific 
(cua-mode t)

;; highlight matching parenthesis
(show-paren-mode 1)

(column-number-mode 1) 

;; don't show tool bar : use more screen space 
(tool-bar-mode -1)

;; modes
(electric-indent-mode 0)

;; global keybindings
(global-unset-key (kbd "C-z"))

;; keep emacs maximised while startup
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; use default font across buffer, replace with your fav one 
(set-frame-font "PragmataPro 16")

;; to enable ligatues for fira code from https://github.com/tonsky/FiraCode/wiki/Setting-up-Emacs
(mac-auto-operator-composition-mode)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

(setq shift-select-mode t)

(load-theme 'ubuntu t)

;; this guy is for fall back 
;; (load-theme 'deeper-blue t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (expand-region alect-themes better-shell use-package undo-tree smooth-scrolling popup-imenu persistent-scratch markdown-mode magit highlight-symbol highlight-indent-guides helm-projectile goto-chg ensime drag-stuff))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
